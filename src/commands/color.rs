use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};



#[command]
async fn color(ctx: &Context, msg: &Message) -> CommandResult {
    let help = "Usage:
!color colorhex      -- set your color in chat. e.g: !color #ffffff, use https://www.color-hex.com/ or something";

    if let Some(caps) = regex!("^!color #([0-9a-f]{6})$")
    .captures(&msg.content)
    .and_then(|res| {res.get(1)}) {
        if let Some(gid) = msg.guild_id {
            if let Some(guild) = ctx.cache.guild(gid).await {

                // if unwrap would be unsound, regex would have failed the parse.
                let colour = u64::from_str_radix(&caps.as_str(), 16).unwrap();

                if let Some(role) = guild.roles.values().find(|&r| {
                    r.name == msg.author.tag()
                }) {
                    role.edit(ctx.http.clone(), |r| {
                        r.colour(colour)
                    }).await?;
                } else {
                    let role = guild.create_role(ctx.http.clone(), |r| {
                        r.mentionable(false)
                        .name(msg.author.tag())
                        .colour(colour)
                    }).await?;
                    let mut member = guild.member(ctx.http.clone(), msg.author.id).await?;
                    member.add_role(ctx.http.clone(), role).await?;
                }
            }
        } else {
            // command came trough pm, has no guildid. prolly do something?
        }
    } else {
        msg.reply(ctx, help).await?;
    };

    Ok(())
}