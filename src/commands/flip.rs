use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

use rand::{Rng, thread_rng};
use rand::distributions::Uniform;

#[command]
async fn flip(ctx: &Context, msg: &Message) -> CommandResult {
    let sides = ["heads", "tails"];

    let res = sides[thread_rng().sample(Uniform::new_inclusive(0, 1))];
    msg.reply(ctx, res).await?;

    Ok(())
}