use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    let help ="Valid commands:
!color colorhex     -- set your color in chat
!help               -- this text
!ping               -- check to see if the bot is responding
!flip               -- flips a coin
!roll [sides]       -- rolls a die, optionally with <sides> sides";

    msg.reply(ctx, help).await?;

    Ok(())
}
