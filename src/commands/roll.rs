use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

use rand::{Rng, thread_rng};
use rand::distributions::Uniform;

#[command]
async fn roll(ctx: &Context, msg: &Message) -> CommandResult {
    let help = "Usage:
!roll               -- rolls a d6
!roll sides         -- rolls a die with <sides> faces, e.g. !roll 20 to roll a d20";

    if let Some(caps) = regex!(r"^!roll ([0-9]+)$")
    .captures(&msg.content)
    .and_then(|val| {val.get(1)}) {
        let high = i32::from_str_radix(caps.as_str(), 10)?;
        let res = thread_rng().sample(Uniform::new_inclusive(1, high)).to_string();
        msg.reply(ctx, res).await?;
    } else if regex!(r"^!roll$").is_match(&msg.content) {
        let res = thread_rng().sample(Uniform::new_inclusive(1, 6)).to_string();
        msg.reply(ctx, res).await?;
    } else {
        msg.reply(ctx, help).await?;
    }
    
    Ok(())
}