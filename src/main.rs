use serenity::async_trait;
use serenity::client::{
    Client,
//    Context,
    EventHandler
};
//use serenity::model::channel::Message;
use serenity::framework::standard::{
    StandardFramework,
    //CommandResult,
    macros::{
        //command,
        group
    }
};

use std::env;

#[macro_use]
extern crate regex_macro;

mod commands;
use commands::{
    color::*,
    flip::*,
    help::*,
    ping::*,
    roll::*
};

#[group]
#[commands(ping, help, color, roll, flip)]
struct General;
struct Handler; 

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .group(&GENERAL_GROUP);

    // Login with a bot token from the environment
    let token = env::var("discord_token").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}


